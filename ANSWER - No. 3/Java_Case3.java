package Case_No3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Java_Case3 {
	public static void main (String[] args){
        String filePath = "C:/input/Data Alert.txt";	// NOTE : Set the Path accordingly to the File Location !!!
        
        Map <String, String> recipients = new HashMap <String, String>();
        Map <String, String> portList = new HashMap <String, String>();
        
        try (BufferedReader br = new BufferedReader (new FileReader (filePath))){
        	String line;
        	
            while ((line = br.readLine()) != null){
                String[] values = line.split (";");
                
                if (recipients.isEmpty() || !recipients.containsKey (values [0])){
                	recipients.put (values [0], values [3]);
                }
                
                portList.put (values [2], values [0]);
            }
            
            Scanner scan = new Scanner (System.in);
            
            String recipient = "";
            
            do {
            	System.out.print ("Recipient Code [Input 0 to Cancel / Exit] : ");
            	recipient = scan.nextLine();
            } while (recipients.get (recipient) == null && !recipient.equals ("0"));
            
            scan.close();
            
            if (!recipient.equals ("0")){
            	printResult (recipient, portList);
            } 
        } catch (IOException e){
            e.printStackTrace();
        }  
	}
	
	public static void printResult (String recipient, Map <String, String> input){
		System.out.println ("\nSelamat Siang Rekan Bank " + recipient + "\n");
    	System.out.println ("Mohon bantuan untuk Sign On pada envi berikut :\n");
    	
    	for (Map.Entry <String, String> ports : input.entrySet()){
    		if (ports.getValue().equals (recipient)){
    			System.out.println ("- Envi MP Port " + ports.getKey() + " terpantau Offline");
    		}
    	}
    	
    	System.out.println ("\nTerima Kasih.");
	}
}