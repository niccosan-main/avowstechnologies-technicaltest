package com.niccosan.avostechnologiestechnicaltest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.niccosan.avostechnologiestechnicaltest.FormObject;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class ApiController {
	@GetMapping
    public String landing (Model model, HttpServletRequest request){
		StringBuffer url = request.getRequestURL();
		String url_path = request.getServletPath();
		String url_base = url.delete (url.length() - url_path.length(), url.length()).toString();
		
		model.addAttribute ("url_path", url_path);
		model.addAttribute ("url_base", url_base);
		
		return "index";
    }
	
	@PostMapping("/form")
    public String index (String requestInput, Model model, HttpServletRequest request){
		StringBuffer url = request.getRequestURL();
		String url_path = request.getServletPath();
		String url_base = url.delete (url.length() - url_path.length(), url.length()).toString();
		
		if (requestInput == null || requestInput.equals ("")){
			requestInput = "No input from User !";
		}

		model.addAttribute ("url_path", url_path);
		model.addAttribute ("url_base", url_base);
        model.addAttribute ("requestInput", requestInput);

        return "index";
    }
	
	@PostMapping("/payment")
    public String payment (String requestInput, Model model, HttpServletRequest request){
		StringBuffer url = request.getRequestURL();
		String url_path = request.getServletPath();
		String url_base = url.delete (url.length() - url_path.length(), url.length()).toString();
		
		if (requestInput == null || requestInput.equals ("")){
			requestInput = "No input from User !";
		}
		
		model.addAttribute ("url_path", url_path);
		model.addAttribute ("url_base", url_base);
        model.addAttribute ("requestInput", requestInput);

        return "index";
    }
	
	@PostMapping("/check")
    public String check (String requestInput, Model model, HttpServletRequest request){
		StringBuffer url = request.getRequestURL();
		String url_path = request.getServletPath();
		String url_base = url.delete (url.length() - url_path.length(), url.length()).toString();
		
		if (requestInput == null || requestInput.equals ("")){
			requestInput = "No input from User !";
		}

		model.addAttribute ("url_path", url_path);
		model.addAttribute ("url_base", url_base);
        model.addAttribute ("requestInput", requestInput);

        return "index";
    }
	
	@PostMapping("/refund")
    public String refund (String requestInput, Model model, HttpServletRequest request){
		StringBuffer url = request.getRequestURL();
		String url_path = request.getServletPath();
		String url_base = url.delete (url.length() - url_path.length(), url.length()).toString();
		
		if (requestInput == null || requestInput.equals ("")){
			requestInput = "No input from User !";
		}

		model.addAttribute ("url_path", url_path);
		model.addAttribute ("url_base", url_base);
        model.addAttribute ("requestInput", requestInput);

        return "index";
    }
	
}