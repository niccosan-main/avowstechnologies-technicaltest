package com.niccosan.avostechnologiestechnicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvostechnologiestechnicaltestApplication {
	public static void main (String[] args){
		SpringApplication.run (AvostechnologiestechnicaltestApplication.class, args);
	}
}