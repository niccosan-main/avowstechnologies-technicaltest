package com.niccosan.avostechnologiestechnicaltest;

public class FormObject {
    private String selectedAction;

    public String getSelectedAction(){
        return selectedAction;
    }

    public void setSelectedAction (String selectedAction){
        this.selectedAction = selectedAction;
    }
}